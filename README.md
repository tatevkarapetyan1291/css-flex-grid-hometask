# CSS-Flex-Grid-Hometask

Create a layout for the header menu based on Flexbox or Grid. The menu and site title "PROJECT" should be situated horizontally, as in the example.

## Getting started

1. cd existing_repo
2. git remote add origin https://gitlab.com/tatevkarapetyan1291/css-flex-grid-hometask.git
3. git branch -M main
4. git push -uf origin main

